import { UsersController } from "../controllers";
import { Router } from "express";
import { AuthController } from "../controllers";
import passport from "passport";


class UserRouter {
  public router: Router;
  private usersController: UsersController;
  private authController: AuthController;
  constructor(usersController: UsersController, authController: AuthController) {
    this.router = Router();
    this.usersController = usersController;
    this.authController = authController;

    this.routes();
  }

  routes() {
    this.router
    .route("/api/auth/register/")
    .post(this.usersController.registration
      /*
        #swagger.tags = ["auth"]
        discription: "Create new user"
          #swagger.requestBody = {
            required: true,
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/registration"
                    }  
                }
            }
          }
      */
      );
    
    this.router
    .route("/api/auth/login/")
    .post(this.authController.login
      /*
        #swagger.tags = ["auth"]
        discription: "Login user"
          #swagger.requestBody = {
            required: true,
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/users"
                    }  
                }
            }
          }
      */
      );

    this.router
    .route("/api/auth/logout/")
    .post(this.authController.logout
      /*
        #swagger.tags = ["auth"]
        discription: "Logout user"
          #swagger.security = [{ "bearerAuth": [] }]
      */
      );

    this.router
    .route("/api/user/")
    .get(passport.authenticate("bearer", { session: false }, ), this.usersController.getList
    /*
      #swagger.tags = ["auth"]
      discription: "Get user info"
      #swagger.security = [{
            "bearerAuth": []
    }]
    */
    )
  }
}

export default UserRouter;
