import NerwpostsRouter from "./newsposts.routes";
import UserRouter from "./user.routes";

export { NerwpostsRouter, UserRouter };
