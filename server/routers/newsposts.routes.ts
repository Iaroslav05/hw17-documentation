import { Router } from "express";
import { BaseController } from "../controllers";

class NerwpostsRouter {
  public router: Router;
  private controller: BaseController;

  constructor(controller: BaseController) {
    this.router = Router();
    this.controller = controller;

    this.routes();
  }
 routes() {
    this.router
      .route("/api/newsposts")
      .post(
        this.controller.create
        /*
        #swagger.tags = ["newsposts"]
          #swagger.requestBody = {
            required: true,
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/newsposts"
                    }  
                }
            }
        } 
    */
      )
      .get(
        this.controller.getList
        //#swagger.tags = ["newsposts"]
      );

    this.router
      .route("/api/newsposts/:id")
      .get(
        this.controller.getSingle
        /* #swagger.params[id] = { name: "id", in: "path", required: true, type: "integer" }
        #swagger.tags = ["newsposts"]
        */
      )
      .put(
        this.controller.update
        /* #swagger.params[id] = { name: "id", in: "path", required: true, type: "integer" }
        #swagger.tags = ["newsposts"]
          #swagger.requestBody = {
            required: true,
            content: {
                "application/json": {
                    schema: {
                        $ref: "#/components/schemas/newsposts"
                    }  
                }
            }
        } 
    */
      )
      .delete(
        this.controller.delete
        /* #swagger.params[id] = { name: "id", in: "path", required: true, type: "integer" }
        #swagger.tags = ["newsposts"]
        */
      );
  }
}

export default NerwpostsRouter;
