// import inMemoryToken from "../service/inMemoryToken";

const useLogout = (): (() => void) => {
  const logout = (): void => {
    localStorage.removeItem("token");
    // onClick={inMemoryToken.deleteToken}
  };

  return logout;
};

export { useLogout };
